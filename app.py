import streamlit as st 

from langchain_community.llms.huggingface_pipeline import HuggingFacePipeline
from langchain_core.prompts import PromptTemplate
#from streamlit_extras.add_vertical_space import add_vertical_space

def select_llm():
    hf = HuggingFacePipeline.from_model_id(
        model_id="gpt2",
        task="text-generation",
        #pipeline_kwargs={"max_new_tokens": 1000},
    )
    return hf

hf = select_llm()

def generate_answer(question):
    template = """Question: {question}

    Answer: Let's think step by step."""
    prompt = PromptTemplate.from_template(template)

    chain = prompt | hf

    question = question
    generated_text = chain.invoke({"question": question})[len("Question: " + question + "\n    Answer: Let's think step by step. "):]

    return generated_text

# App title
st.set_page_config(page_title="🤗💬 IDS701Chat")



with st.sidebar:
   
    st.markdown('''
                
                ## Welcome!
                
                This is a simple chatbot that uses GPT2 to generate responses so it might not perform very well. You can ask any question and the chatbot will try to provide an answer. The chatbot is powered by the Hugging Face library.
                
                ''')
    #add_vertical_space(5)
    st.write('Made with ❤️ by dso12@duke.edu')



# Store LLM generated responses
if "messages" not in st.session_state.keys():
    st.session_state.messages = [{"role": "assistant", "content": "How may I help you?"}]

# Display chat messages
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.write(message["content"])

# User-provided prompt
if prompt := st.chat_input():
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):
        st.write(prompt)

# Generate a new response if last message is not from assistant
if st.session_state.messages[-1]["role"] != "assistant":
    with st.chat_message("assistant"):
        with st.spinner("Thinking..."):
            response = generate_answer(prompt) 
            st.write(response) 
    message = {"role": "assistant", "content": response}
    st.session_state.messages.append(message)

